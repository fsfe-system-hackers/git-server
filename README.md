# FSFE Git Service

[![in docs.fsfe.org](https://img.shields.io/badge/in%20docs.fsfe.org-OK-green)](https://docs.fsfe.org/repodocs/git-server/00_README)

This Ansible playbook takes care of setting up the FSFE's Git Service, currently
based on [Gitea](https://gitea.io). Some sensible bits are encrypted.

* Base setup of host
* MySQL database for gitea
* Gitea configuration
* Webserver (nginx) including some rate limits

## Prerequisites

The machine should be [connected via
innernet](https://git.fsfe.org/fsfe-system-hackers/innernet-playbook) in order
to authenticate users via LDAP. This is not part of this playbook.

## Updates and configuration

Use this playbook to update the configuration of all elements described above.
There is one exception though:

### Upgrading Gitea

First, make a snapshot of `davy` in Proxmox and schedule downtime in the
monitoring system. Before proceeding with the update, also announce the possible
downtime in the team chat. 

To upgrade Gitea itself (in binary form), use `/root/bin/gitea-upgrade.sh` while
providing the desired version. For example:

```
/root/bin/gitea-upgrade.sh 1.13.7
```

This takes care of downloading the binary, checking the hashsum, replacing the
binary, and reminding you of restarting the service.

Afterwards - especially after major upgrades – compare the
[[https://github.com/go-gitea/gitea/tree/master/templates|upstream templates]]
for this release (choose the tag!) with the ones stored in
`roles/gitea/files/custom/templates`. If substantial changes happened with this
release, take the new versions and migrate the custom changes to them. You have
to rerun the playbook (`ansible-playbook playbook -t gitea`) and restart gitea
to put these into effect.
