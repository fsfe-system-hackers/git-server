#!/usr/bin/env bash

VERSION="$1"
BINARY=/usr/local/bin/gitea

if [[ -z "$VERSION" ]]; then
  echo "[ERROR] Provide version"
  exit 1
fi

filename="gitea-$VERSION-linux-amd64"

if [[ -e "${filename}" ]]; then
 rm "${filename}"*
fi

wget -q --show-progress https://dl.gitea.io/gitea/"$VERSION"/"${filename}"{,.sha256} \
  || { echo "[ERROR] Download failed"; rm "${filename}"*; exit 1; }

if sha256sum --check --status "${filename}.sha256"; then
  echo "[INFO] Download successful and hashsums match"
  rm "${filename}.sha256"
  chmod +x "${filename}"
else
  echo "[ERROR] Hashsums don't match!"
  exit 1
fi

read -p "Replace existing gitea binary with ${filename}? [y/N]: " -n 1 -r
if [[ $REPLY =~ ^[Yy]$ ]]; then
  mv -b "${filename}" "${BINARY}" || { echo "[ERROR] Replacing binary failed"; exit 1; }
  echo
  echo "[INFO] Replacing binary was successful."
  read -p "Shall I restart the gitea service? [y/N]: " -n 1 -r
  if [[ $REPLY =~ ^[Yy]$ ]]; then
    echo
    echo "[INFO] Restarting gitea..."
    systemctl restart gitea
  fi
fi
