#!/usr/bin/env bash

# Backup Gitea database
BAKDIR=/root/backup
if [[ ! -e "${BAKDIR}" ]]; then
  mkdir "${BAKDIR}"
fi
mysqldump gitea > "${BAKDIR}"/gitea-db.sql
