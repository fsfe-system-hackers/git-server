#!/usr/bin/env sh

# SPDX-FileCopyrightText: 2020 Free Software Foundation Europe <https://fsfe.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later

pw=$(gpg --batch --use-agent --decrypt ./vaultpw.gpg)

if [ -z "$pw" ]; then
  >&2 echo "Error: vaultpw.gpg is empty or decryption has failed"
  exit 1
else
  >&2 echo "vaultpw.gpg has been decrypted, resulting password is ${#pw} characters long"
  echo "$pw"
  exit 0
fi
